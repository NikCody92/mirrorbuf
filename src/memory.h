#include <sys/types.h>
#include <unistd.h>


#define PAGESIZE sysconf(_SC_PAGESIZE)


int make_shared_area(const char *name, size_t shlen, int *flags);

unsigned char *create_shared_twin_map(int shd, size_t mirdim);
