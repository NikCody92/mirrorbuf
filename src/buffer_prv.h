#include <sys/types.h>
#include <semaphore.h>


struct mirrorbuf {
    char *name;
    size_t len;
    size_t head, tail;
    sem_t readlck, writelck;
    int refcnt;
};
