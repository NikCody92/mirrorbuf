#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#include "memory.h"


int make_shared_area(const char *name, size_t shlen, int *flags)
{
    int shd;
    struct stat shinfo;
    bool exclusive = *flags & O_EXCL;
    errno = 0;
    shd = shm_open(name, *flags | O_EXCL | O_RDWR, 0600);
    if (shd == -1) {
        if (errno == EEXIST) {
            if (exclusive) {
                fprintf(stderr,
                        "The following memory segment cannot be created exclusively: %s..\n", name);
                return -1;
            }
            shd = shm_open(name, *flags | O_RDWR, 0600);
            if (shd == -1) {
                fprintf(stderr, "Unable to create shared memory segment: %s..\n", name);
                return -1;
            }
        } else {
            fprintf(stderr, "Unable to create shared memory segment..\n");
            return -1;
        }
    } else if (!exclusive)
        *flags = *flags | O_EXCL;       /* signal calling function that buffer is being created */
    if (fstat(shd, &shinfo) == -1) {
        fprintf(stderr, "Error while obtaining information about shared memory object..\n");
        if (*flags & O_EXCL)
            if (shm_unlink(name) == -1)
                fprintf(stderr, "Error while unlinking shared memory..\n");
        if (close(shd) == -1)
            fprintf(stderr, "Error while closing shared memory descriptor..\n");
        return -1;
    }
    if (shinfo.st_size != 0 && shinfo.st_size != (off_t) shlen) {
        fprintf(stderr, "Wrong size given..\n");
        if (*flags & O_EXCL)
            if (shm_unlink(name) == -1)
                fprintf(stderr, "Error while unlinking shared memory..\n");
        if (close(shd) == -1)
            fprintf(stderr, "Error while closing shared memory descriptor..\n");
        return -1;
    }
    if (shinfo.st_size == 0)
        if (ftruncate(shd, (off_t) shlen) == -1) {
            fprintf(stderr, "Error while modifying shared memory size..\n");
            if (*flags & O_EXCL)
                if (shm_unlink(name) == -1)
                    fprintf(stderr, "Error while unlinking shared memory..\n");
            if (close(shd) == -1)
                fprintf(stderr, "Error while closing shared memory descriptor..\n");
            return -1;
        }
    return shd;
}

unsigned char *create_shared_twin_map(int shd, size_t mirdim)
{
    unsigned char *mapaddr, *mirr;
    struct stat mapinfo;
    if (fstat(shd, &mapinfo) == -1) {
        fprintf(stderr, "Error while obtaining information about shared memory object..\n");
        return NULL;
    }
    if (mapinfo.st_size != 0 && mapinfo.st_size != (off_t) mirdim) {
        fprintf(stderr, "Wrong size given..\n");
        return NULL;
    }
    if (mapinfo.st_size == 0)
        if (ftruncate(shd, (off_t) mirdim) == -1) {
            fprintf(stderr, "Error while modifying shared memory size..\n");
            return NULL;
        }
    mapaddr = mmap(NULL, 2 * mirdim, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (mapaddr == MAP_FAILED) {
        fprintf(stderr, "Error while obtaining memory..\n");
        return NULL;
    }
    mirr = mmap(mapaddr, mirdim, PROT_READ | PROT_WRITE, MAP_FIXED | MAP_SHARED, shd, 0);
    if (mirr == MAP_FAILED) {
        fprintf(stderr, "Error while creating double mapping..\n");
        if (munmap(mapaddr, 2 * mirdim) == -1)
            fprintf(stderr, "Unable to release map..\n");
        return NULL;
    }
    mirr = mmap(mapaddr + mirdim, mirdim, PROT_READ | PROT_WRITE, MAP_FIXED | MAP_SHARED, shd, 0);
    if (mirr == MAP_FAILED) {
        fprintf(stderr, "Error while creating double mapping..\n");
        if (munmap(mapaddr, 2 * mirdim) == -1)
            fprintf(stderr, "Unable to release map..\n");
        return NULL;
    }
    return mapaddr;
}
