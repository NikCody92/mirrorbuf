#include <stdbool.h>
#include <sys/types.h>
#include "libmirrorbuf/buffer.h"


/** @brief Retrieve a pointer to perform operation on a mirrored buffer.
 *  @param buf Buffer handling structure.
 *  @param pnt Pointer to buffer storage, replaced on return by the requested pointer.
 *  @param read True to perform a read, false to perform a write to the buffer.
 *  @return Amount of data available for the current operation.
 *  
 *  Get a pointer to a location inside the buffer storage, marking the beginning of the area available to the desired operation.
 *  User should access or modify at most the amount of data returned by this function.
 *  Otherwise, memory access errors are likely to occur.
 */
ssize_t mirrorbuf_get_pointer(struct mirrorbuf * buf, unsigned char **pnt, bool read);

/** @brief Commit an operation on a mirrored buffer.
 *  @param buf Buffer handling structure.
 *  @param read True to commit a read, false to commit a write.
 *  @param amnt Amount of data read/written.
 *  @return Whether operation was successfully reported to the buffer.
 *  @remarks No check is done on the given amount, so be careful not to overcommit in order to avoid buffer overflow and memory errors.
 *  
 *  Update buffer information after the described operation has been performed.
 */
bool mirrorbuf_commit_operation(struct mirrorbuf *buf, bool read, size_t amnt);
