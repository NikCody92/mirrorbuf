#include <stdbool.h>
#include <sys/types.h>


/** @brief Mirrored buffer.
 *  
 *  Structure handling allocation and operations on a mirrored buffer.
 */
struct mirrorbuf;

/** @brief Get buffer's name.
 *  @param buf Allocated buffer.
 *  @return Newly allocated copy of the given buffer's name.
 *  
 *  Retrieve name associated with the given buffer at the moment of its creation.
 */
char *mirrorbuf_get_name(struct mirrorbuf *buf);

/** @brief Initialize a mirrored buffer.
 *  @param res Buffer's name (NULL to generate a new random one).
 *  @param len Requested buffer size.
 *  @param[out] blk Pointer to a memory area, containing buffer data, which the user may read or modify directly.
 *  @remarks Resulting size may be greater than requested one due to implementation constraints.
 *  @return Pointer to buffer control structure.
 *  
 *  Allocate space for a buffer and request related resources from the system according to the given flags.
 *  Buffer data will be placed in a different memory area, so that the user may write and read directly from it.
 */
struct mirrorbuf *mirrorbuf_init(const char *res, size_t len, int flags, unsigned char **blk);

/** @brief Unlink buffer resources.
 *  @param res Buffer's name.
 *  @see mirrorbuf_destroy
 *  @remarks It does not free any memory allocated by the related buffer.
 *  @return Whether buffer resources were successfully released.
 *  
 *  Release all resources associated with the buffer identified by the given name.
 *  An initialization with the same name will create a brand new buffer.
 */
bool mirrorbuf_unlink(const char *res);

/** Destroy mirrored buffer.
 *  @param buf Buffer handling structure.
 *  @param data Memory area containing stored data.
 *  @remarks Make sure to keep at least one instance of a buffer, if it has to be retrieved later through its name.
 *  @return Whether the given buffer instance was successfully destroyed.
 *  
 *  Release any memory used by the given buffer for a certain instance in the current process.
 *  If it is the last instance of the buffer with the given name, release all its resources, too, in order to avoid incoherence.
 */
bool mirrorbuf_destroy(struct mirrorbuf *buf, unsigned char *data);
