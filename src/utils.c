#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "memory.h"


#define MIRRORBUF_DIR_TMP "/tmp"


size_t round_to_pagesize(size_t req)
{
    return (req + PAGESIZE - 1) & ~(PAGESIZE - 1);
}

char *string_join(char *first, ...)
{
    char *joined;
    size_t jlen = 1;            /* retain space for string terminator */
    char *iter;
    va_list alst, bckup;
    va_start(alst, first);
#ifdef va_copy
    va_copy(bckup, alst);
#else
    bckup = alst;
#endif
    iter = first;
    while (iter != NULL) {
        jlen += strlen(iter);
        iter = va_arg(alst, char *);
    }
    va_end(alst);
    joined = malloc(jlen);
    if (joined == NULL) {
        fprintf(stderr, "Error while allocating memory..\n");
        va_end(bckup);
        return NULL;
    }
    *joined = '\0';
    iter = first;
    while (iter != NULL) {
        strcat(joined, iter);
        iter = va_arg(bckup, char *);
    }
    va_end(bckup);
    return joined;
}

char *get_tmp_dir(char *hint)
{
    char *dir;
    dir = getenv("TMPDIR");
    if (dir != NULL) {
        return dir;
    }
    if (hint != NULL) {
        return hint;
    }
#ifdef P_tmpdir
    dir = P_tmpdir;
#else
    dir = MIRRORBUF_DIR_TMP;
#endif
    return dir;
}

char *get_tmp_name(const char *prefix)
{
    char *tmpdir = get_tmp_dir(NULL), *tmppath, *tmpname;
    int tmpfd;
    tmppath = string_join(tmpdir, "/", prefix, "XXXXXX", NULL);
    if (tmppath == NULL) {
        fprintf(stderr, "Unable to create temporary name pattern..\n");
        return NULL;
    }
    tmpfd = mkstemp(tmppath);
    if (tmpfd == -1) {
        fprintf(stderr, "Unable to create temporary file..\n");
        return NULL;
    }
    if (close(tmpfd) == -1)
        fprintf(stderr, "Error while closing temporary file.\n");
    tmpname = strdup(strrchr(tmppath, '/') + 1);
    free(tmppath);
    if (tmpname == NULL) {
        fprintf(stderr, "Unable to allocate space for temporary name..\n");
        return NULL;
    }
    return tmpname;
}
