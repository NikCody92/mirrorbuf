#include <sys/types.h>


size_t round_to_pagesize(size_t req);

char *string_join(char *first, ...);

char *get_tmp_name(const char *prefix);
