#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <errno.h>
#include "libmirrorbuf/buffer.h"
#include "libmirrorbuf/ops.h"


#define BUFSIZE 512


struct cpdata {
    bool eof;
    sem_t eoflck;
};

int main(int argc, char *argv[])
{
    int ifd, ofd;
    struct mirrorbuf *mbuf;
    unsigned char *bufdata, *bufpnt;
    char *bname;
    int frkres;
    struct cpdata *rwdata;
    bool end;
    ssize_t dlen, readres = 0, writeres = 0;
    if (argc != 3) {
        fprintf(stderr, "Usage: %s INPUT OUTPUT\n", argv[0]);
        return EXIT_FAILURE;
    }
    ifd = open(argv[1], O_RDONLY);
    if (ifd == -1) {
        fprintf(stderr, "Unable to open input file..\n");
        return EXIT_FAILURE;
    }
    errno = 0;
    ofd = open(argv[2], O_CREAT | O_EXCL | O_TRUNC | O_RDWR, 0644);
    if (ofd == -1) {
        if (errno == EEXIST)
            fprintf(stderr, "Cannot overwrite existing file: %s\n", argv[2]);
        else
            fprintf(stderr, "Unable to open output file..\n");
        if (close(ifd) == -1)
            fprintf(stderr, "Error while closing input file..\n");
        return EXIT_FAILURE;
    }
    mbuf = mirrorbuf_init(NULL, BUFSIZE, O_CREAT, &bufdata);
    if (mbuf == NULL) {
        fprintf(stderr, "Unable to initialize buffer..\n");
        if (close(ifd) == -1 || close(ofd) == -1)
            fprintf(stderr, "Error while closing descriptor..\n");
        return EXIT_FAILURE;
    }
    bname = mirrorbuf_get_name(mbuf);
    if (bname == NULL) {
        fprintf(stderr, "Unable to get buffer name..\n");
        if (close(ifd) == -1 || close(ofd) == -1)
            fprintf(stderr, "Error while closing descriptor..\n");
        return EXIT_FAILURE;
    }
    if (!mirrorbuf_unlink(bname)) {
        fprintf(stderr, "Unable to unlink buffer..\n");
        if (close(ifd) == -1 || close(ofd) == -1)
            fprintf(stderr, "Error while closing descriptor..\n");
        return EXIT_FAILURE;
    }
    rwdata = mmap(NULL, sizeof(struct cpdata), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);      /* simulate shared memory for related processes */
    if (rwdata == MAP_FAILED) {
        fprintf(stderr, "Unable to map shared boolean..\n");
        if (close(ifd) == -1 || close(ofd) == -1)
            fprintf(stderr, "Error while closing descriptor..\n");
        return EXIT_FAILURE;
    }
    rwdata->eof = false;
    if (sem_init(&(rwdata->eoflck), 1, 1) == -1) {
        fprintf(stderr, "Unable to initialize lock for EOF..\n");
        if (close(ifd) == -1 || close(ofd) == -1)
            fprintf(stderr, "Error while closing descriptor..\n");
        return EXIT_FAILURE;
    }
    frkres = fork();
    if (frkres == -1) {
        fprintf(stderr, "Error while forking process..\n");
        if (close(ifd) == -1 || close(ofd) == -1)
            fprintf(stderr, "Error while closing descriptor..\n");
        return EXIT_FAILURE;
    } else if (frkres != 0) {
        if (close(ifd) == -1)
            fprintf(stderr, "Error while closing descriptor..\n");
        dlen = 0;
        while (dlen == 0 || writeres > 0) {
            if (dlen > 0)
                if (!mirrorbuf_commit_operation(mbuf, 1, writeres)) {
                    fprintf(stderr, "Unable to commit buffer read..\n");

                    return EXIT_FAILURE;
                }
            do {
                usleep(1000000);
                printf("Reading buffer..\n");
                bufpnt = bufdata;
                dlen = mirrorbuf_get_pointer(mbuf, &bufpnt, 1);
                if (sem_wait(&(rwdata->eoflck)) == -1) {
                    fprintf(stderr, "Error while waiting for EOF..\n");
                    if (close(ofd) == -1)
                        fprintf(stderr, "Error while closing descriptor..\n");
                    return EXIT_FAILURE;
                }
                end = rwdata->eof;
                if (sem_post(&(rwdata->eoflck)) == -1)
                    fprintf(stderr, "Error while releasing lock for EOF..\n");
            } while (dlen == 0 && !end);
            if (dlen == -1) {
                fprintf(stderr, "Unable to get buffer pointer..\n");
                if (close(ofd) == -1)
                    fprintf(stderr, "Error while closing descriptor..\n");
                return EXIT_FAILURE;
            }
            if (end)
                break;
            writeres = write(ofd, bufpnt, dlen);
        }
        if (close(ofd) == -1)
            fprintf(stderr, "Error while closing output file..\n");
        if (writeres == -1) {
            fprintf(stderr, "Error while writing: %s..\n", strerror(errno));
            return EXIT_FAILURE;
        }
    } else {
        dlen = 0;
        while (dlen == 0 || readres > 0) {
            if (dlen > 0)
                if (!mirrorbuf_commit_operation(mbuf, 0, readres)) {
                    fprintf(stderr, "Unable to commit buffer read..\n");
                    return EXIT_FAILURE;
                }
            do {
                usleep(1000000);
                printf("Writing buffer..\n");
                bufpnt = bufdata;
                dlen = mirrorbuf_get_pointer(mbuf, &bufpnt, 0);
            } while (dlen == 0);
            if (dlen == -1) {
                fprintf(stderr, "Unable to get buffer pointer..\n");
                return EXIT_FAILURE;
            }
            readres = read(ifd, bufpnt, dlen);
        }
        if (close(ifd) == -1)
            fprintf(stderr, "Error while closing input file..\n");
        if (sem_wait(&(rwdata->eoflck)) == -1) {
            fprintf(stderr, "Error while waiting for EOF..\n");
            if (close(ofd) == -1)
                fprintf(stderr, "Error while closing descriptor..\n");
            return EXIT_FAILURE;
        }
        rwdata->eof = true;
        if (sem_post(&(rwdata->eoflck)) == -1)
            fprintf(stderr, "Error while releasing lock for EOF..\n");
        if (readres == -1) {
            fprintf(stderr, "Error while reading: %s..\n", strerror(errno));
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}
