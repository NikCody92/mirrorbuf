#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <semaphore.h>
#include "utils.h"
#include "memory.h"
#include "buffer_prv.h"

#define PREFIX_POSIX_SHM "/"

#define MIRRORBUF_SUFFIX_HEADER "header"
#define MIRRORBUF_SUFFIX_BLOCK "data"
#define MIRRORBUF_SUFFIX_REF_LOCK "reflck"
#define MIRRORBUF_PREFIX_TEMP_RES "mirrorbuf"


char *mirrorbuf_get_name(struct mirrorbuf *buf)
{
    return strdup(buf->name);
}

sem_t *open_ref_lock(const char *res)
{
    sem_t *lck;
    char *lckname;
    lckname = string_join(PREFIX_POSIX_SHM, res, ".", MIRRORBUF_SUFFIX_REF_LOCK, NULL);
    if (lckname == NULL) {
        fprintf(stderr, "Unable to create name for reference locking semaphore..\n");
        return SEM_FAILED;
    }
    lck = sem_open(lckname, O_CREAT, 0600, 1);
    if (lck == SEM_FAILED) {
        fprintf(stderr, "Unable to create reference locking semaphore..\n");
        return SEM_FAILED;
    }
    return lck;
}

struct mirrorbuf *mirrorbuf_init_header(const char *res, int flags)
{
    struct mirrorbuf *buf;
    char *headname;
    int headfd;
    sem_t *reflck;
    headname = string_join(PREFIX_POSIX_SHM, res, ".", MIRRORBUF_SUFFIX_HEADER, NULL);
    if (headname == NULL) {
        fprintf(stderr, "Unable to create buffer name..\n");
        return NULL;
    }
    reflck = open_ref_lock(res);
    if (reflck == SEM_FAILED) {
        fprintf(stderr, "Unable to get reference lock..\n");
        return NULL;
    }
    if (sem_wait(reflck) == -1) {
        fprintf(stderr, "Error while waiting for reference..\n");
        return NULL;
    }
    headfd = make_shared_area(headname, sizeof(struct mirrorbuf), &flags);
    free(headname);
    if (headfd == -1) {
        fprintf(stderr, "Unable to create shared area..\n");
        return NULL;
    }
    buf = mmap(NULL, sizeof(struct mirrorbuf), PROT_READ | PROT_WRITE, MAP_SHARED, headfd, 0);
    if (close(headfd) == -1)
        fprintf(stderr, "Error while closing descriptor for shared memory area..\n");
    if (buf == NULL) {
        fprintf(stderr, "Unable to allocate space for buffer..\n");
        return NULL;
    }
    if (flags & O_EXCL) {       /* if buffer is being created */
        memset(buf, 0, sizeof(struct mirrorbuf));
        buf->name = strdup(res);
        if (buf->name == NULL) {
            fprintf(stderr, "Unable to allocate space for buffer name..\n");
            return NULL;
        }
        if (sem_init(&(buf->readlck), 1, 1) == -1) {
            fprintf(stderr, "Unable to initialize read contention semaphore..\n");
            return NULL;
        }
        if (sem_init(&(buf->writelck), 1, 1) == -1) {
            fprintf(stderr, "Unable to initialize write contention semaphore..\n");
            sem_destroy(&(buf->readlck));
            return NULL;
        }
        buf->refcnt = 1;
    } else
        buf->refcnt++;
    if (sem_post(reflck) == -1 || sem_close(reflck) == -1)
        fprintf(stderr, "Error while releasing reference..\n");
    return buf;
}

unsigned char *mirrorbuf_init_block(const char *res, size_t blen)
{
    unsigned char *blk;
    char *blkname;
    int blkfd, flags = O_CREAT;
    blkname = string_join(PREFIX_POSIX_SHM, res, ".", MIRRORBUF_SUFFIX_BLOCK, NULL);
    if (blkname == NULL) {
        fprintf(stderr, "Unable to create name for buffer data..\n");
        return false;
    }
    blkfd = make_shared_area(blkname, blen, &flags);
    if (blkfd == -1) {
        fprintf(stderr, "Unable to create area for buffer block..\n");
        return false;
    }
    blk = create_shared_twin_map(blkfd, blen);
    if (close(blkfd) == -1)
        fprintf(stderr, "Error while closing descriptor for shared memory area..\n");
    if (blk == NULL) {
        fprintf(stderr, "Unable to create mirrored mapping..\n");
        return false;
    }
    return blk;
}

/** Initialize ring buffer.
 *  @param res Buffer name used to allocate internal resources.
 *  @param len Size for the new buffer.
 *  @param flags Flags to pass to open shared memory.
 *  @param[out] blk A shared memory area allocated for buffer data.
 *  @remarks A name should be given if the buffer has to be shared among different processes.
 *  @remarks Size of the created buffer will be rounded up to a multiple of the page size.
 *  
 *  Allocate space for buffer and its resources.
 *  If no buffer name is given, generate a new random name.
 */
struct mirrorbuf *mirrorbuf_init(const char *res, size_t len, int flags, unsigned char **blk)
{
    struct mirrorbuf *buf;
    size_t blen = round_to_pagesize(len);
    if (res == NULL) {
        res = get_tmp_name(MIRRORBUF_PREFIX_TEMP_RES);
        if (res == NULL) {
            fprintf(stderr, "Unable to get new resource name..\n");
            return NULL;
        }
    }
    buf = mirrorbuf_init_header(res, flags);
    if (buf == NULL) {
        fprintf(stderr, "Unable to initialize buffer header..\n");
        return NULL;
    }
    if (buf->len != 0 && buf->len != blen) {
        fprintf(stderr, "Incoherent size given for the existing buffer..\n");
        return NULL;
    }
    buf->len = blen;
    *blk = mirrorbuf_init_block(res, blen);
    if (*blk == NULL) {
        fprintf(stderr, "Unable to get shared area for buffer data..\n");
        return NULL;
    }
    return buf;
}

bool mirrorbuf_unlink(const char *res);

bool mirrorbuf_destroy(struct mirrorbuf *buf, unsigned char *data)
{
    sem_t *reflck;
    reflck = open_ref_lock(buf->name);
    if (reflck == SEM_FAILED) {
        fprintf(stderr, "Unable to get reference lock..\n");
        return false;
    }
    if (sem_wait(reflck) == -1) {
        fprintf(stderr, "Error while waiting for reference..\n");
        return false;
    }
    if (munmap(data, 2 * buf->len) == -1) {
        fprintf(stderr, "Error while unmapping buffer data..\n");
        return false;
    }
    buf->refcnt--;
    if (buf->refcnt == 0) {
        if (sem_destroy(&(buf->readlck)) == -1 || sem_destroy(&(buf->writelck)) == -1)
            fprintf(stderr, "Error while releasing semaphores..\n");
        if (!mirrorbuf_unlink(buf->name))       /* trigger buffer unlinking since header information will be incoherent */
            fprintf(stderr, "Unable to unlink buffer..\n");
        free(buf->name);
    }
    if (munmap(buf, sizeof(struct mirrorbuf)) == -1) {
        fprintf(stderr, "Error while unmapping buffer header..\n");
        return false;
    }
    if (sem_post(reflck) == -1 || sem_close(reflck) == -1)
        fprintf(stderr, "Error while releasing reference..\n");
    return true;
}

bool mirrorbuf_unlink(const char *res)
{
    char *headname, *blkname;
    headname = string_join(PREFIX_POSIX_SHM, res, ".", MIRRORBUF_SUFFIX_HEADER, NULL);
    blkname = string_join(PREFIX_POSIX_SHM, res, ".", MIRRORBUF_SUFFIX_BLOCK, NULL);
    if (headname == NULL || blkname == NULL) {
        fprintf(stderr, "Unable to create name for resources to unlink..\n");
        if (headname != NULL)
            free(headname);
        return false;
    }
    if (shm_unlink(headname) == -1 || shm_unlink(blkname) == -1) {
        fprintf(stderr, "Unable to unlink resources..\n");
        free(blkname);
        free(headname);
        return false;
    }
    free(blkname);
    free(headname);
    return true;
}
