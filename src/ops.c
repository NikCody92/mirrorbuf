#include <stdbool.h>
#include <stdio.h>
#include <semaphore.h>
#include "buffer_prv.h"


ssize_t mirrorbuf_get_pointer(struct mirrorbuf * buf, unsigned char **pnt, bool read)
{
    size_t datalen;
    if (sem_wait(&(buf->readlck)) == -1) {
        fprintf(stderr, "Error while locking for read..\n");
        return -1;
    }
    if (sem_wait(&(buf->writelck)) == -1) {
        fprintf(stderr, "Error while locking for write..\n");
        if (sem_post(&(buf->readlck)) == -1)
            fprintf(stderr, "Error while releasing lock for read..\n");
        return -1;
    }
    if (buf->tail >= buf->len) {
        buf->head -= buf->len;
        buf->tail -= buf->len;
    }
    datalen = buf->head - buf->tail;
    if (read)
        *pnt = *pnt + buf->tail;
    else {
        *pnt = *pnt + buf->head;
        datalen = buf->len - datalen;
    }
    if (sem_post(&(buf->writelck)) == -1)
        fprintf(stderr, "Error while releasing lock for write..\n");
    if (sem_post(&(buf->readlck)) == -1)
        fprintf(stderr, "Error while releasing lock for read..\n");
    return (ssize_t) datalen;
}

bool mirrorbuf_commit_operation(struct mirrorbuf * buf, bool read, size_t amnt)
{
    sem_t *lck;
    if (read)
        lck = &(buf->readlck);
    else
        lck = &(buf->writelck);
    if (sem_wait(lck) == -1) {
        fprintf(stderr, "Error while waiting for buffer lock..\n");
        return false;
    }
    if (read)
        buf->tail += amnt;
    else
        buf->head += amnt;
    if (sem_post(lck) == -1)
        fprintf(stderr, "Error while releasing buffer lock..\n");
    return true;
}
